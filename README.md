Dude that likes to work with hardware, firmware and low level stuff

_The gears turn once more..._


**NOTE**: This is read-only profile. All changes are pushed from repos on private git server to this public github account. 

## Identity:


#### Public key:

![Public key QR code](xunillen.pub.png)

```
untrusted comment: signify public key
RWS8/mrTTyKER42KjBPnoEKprKjRV5mKa1K7o4/qE3TwIl5bqX8GPgQc
```

#### Public ed25519 key:

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHmZkpeNmmuOJRmyh+Eo3SrUNcovaFZVxXbPBuqVXzWL xunillen2@proton.me
```

#### Public ed25519 sha256:

```
SHA256 (xunillen_key.pub) = 0f97c80adf0630fd9b3ca4e15cc96bc672b931ceb949b77925499e1e39f8ea85
```

#### Public ed25519 key signature:

```
RWS8/mrTTyKER671bB2tL3Ld+hwEuBy0Ea8fcEemb5EkSmIFHd49lFEltKjMmW8t+Xwvk7ZUjbCPrITpoA9Rr4+jT/nxs64OpQs=
```

